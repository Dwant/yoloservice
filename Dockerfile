FROM gcr.io/tensorflow/tensorflow

RUN apt-get update && apt-get install -y --no-install-recommends \
		ca-certificates \
		libsqlite3-0 \
		libssl1.0.0 \
	&& rm -rf /var/lib/apt/lists/*

## install opencv

RUN apt-get update && \
        apt-get install -y \
        build-essential \
        cmake \
        git \
        wget \
        unzip \
        yasm \
        pkg-config \
        libswscale-dev \
        libtbb2 \
        libtbb-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libjasper-dev \
        libavformat-dev \
        libpq-dev


WORKDIR /
RUN wget https://github.com/Itseez/opencv/archive/3.0.0.zip \
&& unzip 3.0.0.zip \
&& mkdir /opencv-3.0.0/cmake_binary \
&& cd /opencv-3.0.0/cmake_binary \
&& cmake -DBUILD_TIFF=ON \
  -DBUILD_opencv_java=OFF \
  -DWITH_CUDA=OFF \
  -DENABLE_AVX=ON \
  -DWITH_OPENGL=ON \
  -DWITH_OPENCL=ON \
  -DWITH_IPP=ON \
  -DWITH_TBB=ON \
  -DWITH_EIGEN=ON \
  -DWITH_V4L=ON \
  -DBUILD_TESTS=OFF \
  -DBUILD_PERF_TESTS=OFF \
  -DCMAKE_BUILD_TYPE=RELEASE \
  -DCMAKE_INSTALL_PREFIX=$(python -c "import sys; print(sys.prefix)") \
  -DPYTHON_EXECUTABLE=$(which python) \
  -DPYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
  -DPYTHON_PACKAGES_PATH=$(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") .. \
&& make install \
&& rm /3.0.0.zip \
&& rm -r /opencv-3.0.0

RUN pip install flake8 pep8 --upgrade
## install flask
RUN pip install flask

RUN apt-get install python-gevent -y



WORKDIR "/notebooks"


# Install Java for jenkins slave
RUN \
  apt-get update && \
  apt-get install -y openjdk-7-jdk && \
rm -rf /var/lib/apt/lists/*

# sshd
RUN apt-get update && apt-get install -y openssh-server
RUN sed -i 's|session    required     pam_loginuid.so|session    optional     pam_loginuid.so|g' /etc/pam.d/sshd
RUN echo "PermitUserEnvironment yes" >> /etc/ssh/sshd_config

RUN mkdir /var/run/sshd
ADD setup-sshd /usr/local/bin/setup-sshd
RUN chmod +x /usr/local/bin/setup-sshd

ENV HOME /home/jenkins
RUN useradd -m -d $HOME -s /bin/sh jenkins &&\
    echo "jenkins:jenkins" | chpasswd
RUN echo "jenkins ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

EXPOSE 22

COPY yoloservice /home/jenkins
RUN chown -R jenkins:jenkins /home/jenkins/*

CMD ["/usr/local/bin/setup-sshd"]
