from gevent.wsgi import WSGIServer
from yolo_flask_service import app

http_server = WSGIServer(('', 9000), app)
http_server.serve_forever()