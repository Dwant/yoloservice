 #!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from flask import Flask, request, redirect, url_for, render_template,            send_from_directory
from flask import abort
from flask import flash
                    
from werkzeug.utils import secure_filename
from flask import request


import cv2
from yoloface import YOLO_TF

app = Flask(__name__, static_url_path='')
yolo = YOLO_TF()

@app.route('/')
def hello_world():
    return 'Hello, World!'

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
UPLOAD_FOLDER = os.path.join(os.path.dirname(__file__), "uploads")
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''

@app.route('/show/<filename>')
def uploaded_file(filename):
    return render_template('response_template.html', filename = filename)

@app.route('/uploads/<filename>')
def serve_image(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], 
                               filename)


@app.route('/detectface', methods=["GET", "POST"])
def detect_face():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            
        if file and allowed_file(file.filename):
            
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('serve_results',
                                    filename=filename))
        
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''


@app.route('/detectface/result/<filename>')
def serve_results(filename):
    
    path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    
    app.logger.info('serving: %s' % path)
    img = cv2.imread(path)
        
    faces = yolo.detect_faces_from_file(path, 0.1, False)

    if len(faces) == 0:
        return None

    if len(faces) > 1:
        for (y, y2, x, x2) in faces:
            print((x, y, x2, y2))
            img_ = img[y:y2, x:x2]
            break
    else:
        return None

    detection_filename = 'res_' + filename
    
    res = cv2.imwrite(os.path.join(app.config['UPLOAD_FOLDER'], detection_filename), img_)
    
    
    return render_template('response_template.html', origin = filename, detection = detection_filename)

from flask import jsonify
import tempfile
import os

@app.route('/api/detectface', methods=["POST"])
def api_detect_face():
      
    if request.method == 'POST':
        if 'file' not in request.files:
            app.logger.error("no file")
            return abort(500)
        
        file = request.files['file']    
        if file:
            is_detected = False
            bbox = None
            
            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            app.logger.info('process %s' % filepath)
            file.save(filepath)
            faces = yolo.detect_faces_from_file(filepath, 0.1, False)
            if len(faces) > 1:
                bbox = faces[0]
                is_detected = True
                
            os.remove(filepath)

            return jsonify(is_detected=is_detected, bbox=bbox)
    
    app.logger.error("only POST method, but get %s" % request.method)
    return abort(500)


if __name__ == "__main__":
    app.secret_key = "why?"
    app.run(host="0.0.0.0", port=9000, debug=True, threaded=True)




               

